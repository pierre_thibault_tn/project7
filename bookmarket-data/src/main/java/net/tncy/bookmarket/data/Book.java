package net.tncy.bookmarket.data;

public class Book
{
	private int id;
	private String title;
	private String author;
	private String publisher;

	@ISBN
	private String isbn;

	public Book(int id, String title, String author, String publisher) 
	{
		this.id = id;
		this.title = title;
		this.author = author;
		this.publisher = publisher;
	}

	public int getId() 
	{
		return this.id;
	}

	public String getTitle() 
	{
		return this.title;
	}

	public String getAuthor() 
	{
		return this.author;
	}

	public String getPublisher()
	{
		return this.publisher;
	}
}