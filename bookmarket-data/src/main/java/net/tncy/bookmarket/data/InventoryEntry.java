package net.tncy.bookmarket.data;

public class InventoryEntry
{
	private Book book;
	private int quantity;
	private float averagePrice;
	private float currentPrice;

	public InventoryEntry(Book book, int quantity, float averagePrice, float currentPrice) 
	{
		this.book = book;
		this.quantity = quantity;
		this.averagePrice = averagePrice;
		this.currentPrice = currentPrice;
	}
}